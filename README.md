# Helpdesk client api demonstration
Requires requests==2.13.0 to be installed

Variables HELPDESK_HOST, HELPDESK_USER_1_API_KEY and HELPDESK_MODERATOR_API_KEY in code should be modified.

Example:
```python
HELPDESK_HOST = 'http://127.0.0.1:8000'
HELPDESK_USER_1_API_KEY = 'acb181affd732277bb0794b94b305cdd35af92c1'
HELPDESK_MODERATOR_API_KEY = '8da098a995b3f82eb86a4aecc4f5c36c4bf14125'
```