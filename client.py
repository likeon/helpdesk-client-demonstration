import requests
from pprint import pprint

HELPDESK_HOST = ''

HELPDESK_USER_1_API_KEY = ''
HELPDESK_MODERATOR_API_KEY = ''

# API expects api key to be in query params

# Users can create topics
requests.post(
    '%s/api/helpdesk/topics/?api_key=%s' % (HELPDESK_HOST, HELPDESK_USER_1_API_KEY),
    data={
        'subject': 'My PC isn\'t working',
        'message': 'HELP ME!'
    },
)

# They can also observe a list of topics they created
user1_topics = requests.get(
    '%s/api/helpdesk/topics/?api_key=%s' % (HELPDESK_HOST, HELPDESK_USER_1_API_KEY),
).json()
pprint(user1_topics)

# Moderators can see the whole list of topics
whole_list_of_topics = requests.get(
    '%s/api/helpdesk/topics/?api_key=%s' % (HELPDESK_HOST, HELPDESK_MODERATOR_API_KEY),
).json()
pprint(whole_list_of_topics)

# Moderators suppose to solve user's problems
a_topic_from_the_list = whole_list_of_topics[0]
pprint(a_topic_from_the_list)

# Detail view also provides the whole correspondence of the topic
topic_detail = requests.get(
    '%s/api/helpdesk/topics/%d/?api_key=%s' % (
        HELPDESK_HOST,
        a_topic_from_the_list['id'],
        HELPDESK_MODERATOR_API_KEY
    ),
)
pprint(topic_detail.text)

# Moderator can post a comment to a user
requests.post(
    '%s/api/helpdesk/topics/%d/post-comment/?api_key=%s' % (
        HELPDESK_HOST,
        a_topic_from_the_list['id'],
        HELPDESK_MODERATOR_API_KEY
    ),
    data={
        'message': 'Problem solved'
    },
).json()

# User can post a comment to a moderator
requests.post(
    '%s/api/helpdesk/topics/%d/post-comment/?api_key=%s' % (
        HELPDESK_HOST,
        a_topic_from_the_list['id'],
        HELPDESK_USER_1_API_KEY,
    ),
    data={
        'message': 'Thank you'
    },
).json()

# Also topics have text labels, that could be set up
requests.post(
    '%s/api/helpdesk/topics/%d/labels/?api_key=%s' % (
        HELPDESK_HOST,
        a_topic_from_the_list['id'],
        HELPDESK_USER_1_API_KEY,
    ),
    data={
        'name': 'Lllllable'
    },
)

# Or deleted
requests.delete(
    '%s/api/helpdesk/topics/%d/labels/?api_key=%s' % (
        HELPDESK_HOST,
        a_topic_from_the_list['id'],
        HELPDESK_USER_1_API_KEY,
    ),
    data={
        'name': 'Lllllable'
    },
)

# By both moderator and user


# Topics could be deactivated
# This way they won't be shown in any listings and access for them would be denied
